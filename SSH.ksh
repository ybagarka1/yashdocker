#!/bin/sh
##Author Yash Bagarka ##
##Script to accept hostname as argument


#Getting all host name seperated by comma from command line

oIFS="$IFS"; 
IFS=, ; 
set -- $1 ; 
IFS="$oIFS"

if [ $# == 0 ]
then
	echo "Enter at least one argument"
	exit
fi



#The prompt(cmd) from user to display the result post connection
echo -n "prompt to enter the command to be ran on all host connected: "
    read cmd
		echo "The command entered by you is" $cmd
		
	
#Assuming root user for password less connectivity
for i in "$@"; do
    hostname=$i
  echo Connected to $hostname
  ssh root@$hostname
  $cmd
  done
  	
echo entered host names are : $#
